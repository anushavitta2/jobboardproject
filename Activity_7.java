package TestNGActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_7 {
	WebDriver driver;
	 WebDriverWait wait;
	 
	  @BeforeMethod
	  public void beforeMethod() {
		  
				//Create a new instance of the Firefox driver
				driver = new FirefoxDriver();
				
				 //Open browser
				driver.get("https://alchemy.hguy.co/jobs");	
				    }
	 	
	
  @Test
  
  public void activity_7() throws InterruptedException {
	  //Post a Job
	  WebElement postJob = driver.findElement(By.xpath("//a[contains(text(),'Post a Job')]"));
	  postJob.click();
	  //details to be filled and click preview and submit the Job
	  driver.findElement(By.cssSelector("a.button")).click();
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  driver.findElement(By.id("wp-submit")).click();
		driver.findElement(By.id("job_title")).sendKeys("Senior Testing");
	    driver.findElement(By.id("job_location")).sendKeys("India");
	    driver.findElement(By.id("job_type")).sendKeys("Full Time");
	    driver.findElement(By.id("job_description_ifr")).sendKeys("Job Description");
	    driver.findElement(By.id("application")).clear();
	    driver.findElement(By.id("application")).sendKeys("anunethi2@gmail.com");
	    driver.findElement(By.xpath("//input[@name='submit_job']")).click();
	    Thread.sleep(10000);
	    driver.findElement(By.xpath("//input[@id='job_preview_submit_button']")).click();
           
  }

@AfterMethod
public void afterMethod() {
	//Close the browser
    driver.quit();
}
}
