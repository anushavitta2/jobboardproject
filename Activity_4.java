package TestNGActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_4 {
WebDriver driver;
	
	@BeforeMethod
	  public void beforeMethod() {
		//Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		
		 //Open browser
		driver.get("https://alchemy.hguy.co/jobs");	
		    }
	
  @Test
  public void Activity4() {
	  
	// Check the heading of the page
	  String secondHeading = driver.findElement(By.xpath("//h2[contains(text(),'Quia quis non')]")).getText();
	System.out.println ("Heading of the Page is : " + secondHeading);
	
	//Assertion for page title
    Assert.assertEquals("Quia quis non", secondHeading);    
  }
  
  @AfterMethod
  public void afterMethod() {
	  //Close the browser
      driver.quit();
  }
}
