package TestNGActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_3 {
	WebDriver driver;
	
	@BeforeMethod
	  public void beforeMethod() {
		//Create a new instance of the Firefox driver
				driver = new FirefoxDriver();
				
				 //Open browser
				driver.get("https://alchemy.hguy.co/jobs");	
				    }
	 
  @Test
  public void Activity3() {
	  
	  //Get the url of the header image
	  
	  
	  	 // WebElement url = driver.findElement(By.xpath("//img[@class='attachment-large size-large wp-post-image']"));
	  //List<WebElement> listImages = driver.findElements(By.tagName("img"));
	 // System.out.println("List of images is" +listImages.size());
	 
	 
	  
	  WebElement url = driver.findElement(By.tagName("img"));
	  //driver.get("https://alchemy.hguy.co/job");
	 String src = url.getAttribute("src");
	 
	  //Print the url to the console
	   
  System.out.println("URL of the header image is " +src);
 	  
  }
  

  @AfterMethod
  public void afterMethod() {
	//Close the browser
      driver.quit();
    }

}
