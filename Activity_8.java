package TestNGActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity_8 {
	
	WebDriver driver;
		
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  }

	
  @Test
  public void activity_8() {
	  
	  //ENter User name and password fields
	  driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
         //Get the title
		String title = driver.getTitle();
		System.out.println("Title of the page is " +title);
		
  }


@AfterMethod
public void afterMethod() {
	//Close the browser
    driver.quit();
}
}
