package TestNGActivities;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;


public class Activity_10 {
WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  }

  @Test
  public void activity_10 () throws InterruptedException  {
	  //ENter User name and password fields
	  driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Click on the "Users" menu item
				driver.findElement(By.cssSelector("a.menu-icon-users")).click();

		//users.click();
		Thread.sleep(10000);
		
		// Click on the "Add New" button
				driver.findElement(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")).click();

		//driver.findElement(By.xpath("//li[@class='current']")).click();
		//driver.findElement(By.tagName("a")).click();
		//driver.findElement(By.linkText("Add New")).click();
		//driver.findElement(By.xpath("//li[@id='menu-users']//ul[@class='wp-submenu wp-submenu-wrap']//li//a[contains(text(),'Add New')]")).click();
		driver.findElement(By.id("user_login")).sendKeys("anuvitta");
		driver.findElement(By.id("email")).sendKeys("anuvitta@in.ibm.com");
		driver.findElement(By.id("first_name")).sendKeys("Anusha");
		driver.findElement(By.id("last_name")).sendKeys("Vitta");
		driver.findElement(By.id("url")).sendKeys("www.ibm.com");
		driver.findElement(By.id("createusersub")).click();
		driver.findElement(By.cssSelector(".wp-generate-pw")).click();
		driver.findElement(By.id("createusersub")).click();
		WebElement message =  driver.findElement(By.cssSelector("#message > p:nth-child(1)"));
		 System.out.println("messageText :"+message.getText()); 
		 message.click();
		 
		 //verify that user created
		 String actual = message.getText();
		 String expected = "New user created. Edit user";
		 Assert.assertEquals(actual, expected);
		 
		
  }

@AfterMethod
public void afterMethod() {
 	//Close the browser
     driver.quit();
 }
}

