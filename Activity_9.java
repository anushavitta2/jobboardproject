package TestNGActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity_9 {
	
	WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  }

	
	
  @Test
  public void activity_9() {
	 

	  //ENter User name and password fields
	  driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
		
		//Job listings
		WebElement joblisting = driver.findElement(By.xpath("//div[contains(text(),'Job Listings')]"));
		joblisting.click();
		
		 List<WebElement> joblist = driver.findElements(By.tagName("li"));
		   System.out.println("results of the list is :" +joblist.size()); 
		   for (int i=0;i<joblist.size();i++)
			 {
				System.out.println(joblist.get(i).getText());
				
			if(joblist.get(i).getText().contains("Add New"))
			{
			joblist.get(i).click();
			
					break;
				} 
			}
		   driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		//Add new Button
		
		//driver.findElement(By.linkText("Add New")).click();
		WebElement title = driver.findElement(By.id("post-title-0"));
		title.click();
		title.sendKeys("Senior Tester");
		driver.findElement(By.id("_job_location")).sendKeys("India");
		   driver.findElement(By.id("_application")).clear();
		   driver.findElement(By.id("_application")).sendKeys("anunethi2@gmail.com");
		   driver.findElement(By.id("_company_name")).sendKeys("IBM");
		   driver.findElement(By.id("_company_website")).sendKeys("www.ibm.com");
		   driver.findElement(By.id("_company_tagline")).sendKeys("THINK");
		   driver.findElement(By.id("_company_twitter")).sendKeys("@ibm_in");
		   driver.findElement(By.id("_company_video")).sendKeys("www.ibm.com");
		   //Publish the Job
		   WebElement publishButton= driver.findElement(By.cssSelector(".editor-post-publish-panel__toggle"));
		   publishButton.click();	
		   WebElement publishButton1 = driver.findElement(By.cssSelector(".editor-post-publish-button"));
		   publishButton1.click();
  }

		   @AfterMethod
 public void afterMethod() {
  	//Close the browser
      driver.quit();
  }
}
