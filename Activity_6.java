package TestNGActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_6 {
	WebDriver driver;
	 WebDriverWait wait;
	 
	  @BeforeMethod
	  public void beforeMethod() {
		  
				//Create a new instance of the Firefox driver
				driver = new FirefoxDriver();
				
				 //Open browser
				driver.get("https://alchemy.hguy.co/jobs");	
				    }
	  
	  @Test
	  public void activity_6 () {
		  //find the navigation bar and click on Jobs
		  
		  WebElement  Jobs = driver.findElement(By.cssSelector("body"));
		  
		  Jobs.click();
				  
	//read the Page Title
		  
		  String pageTitle = driver.findElement(By.className("site-description")).getText();
		  
		  System.out.println("Title of the page is " +pageTitle);
		 
		  //Search for a Job
		  WebElement job=  driver.findElement(By.xpath("//input[@id='search_keywords']"));
		  job.sendKeys("Senior tester");
		  
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  // Wait for the Job listings and submit
		  
		 WebElement submit = driver.findElement(By.xpath("//input[contains(@type,'submit')]"));
		  submit.click();
		  driver.manage().window().maximize();
		  
		  //Apply for the  job from the listings
		  
		  WebElement applyJob = driver.findElement(By.className("application_button button"));
		  applyJob.click();
		  
		  //Print the Email 
		  WebElement JobapplEmail = driver.findElement(By.cssSelector(".job_application_email"));
		   System.out.println("job_application_email:" +JobapplEmail.getText());
		  	} 
	  

	  @AfterMethod
	  public void afterMethod() {
		//Close the browser
	      driver.quit();
	  }
	  
}
