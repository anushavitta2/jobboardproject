package TestNGActivities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_5 {
	WebDriver driver;
  
  @BeforeMethod
  public void beforeMethod() {
	  
			//Create a new instance of the Firefox driver
			driver = new FirefoxDriver();
			
			 //Open browser
			driver.get("https://alchemy.hguy.co/jobs");	
			    }
  
  @Test
  public void f() {
	  //find the navigation bar and click on Jobs
	  
	  WebElement  Jobs = driver.findElement(By.cssSelector("body"));
	  
	  Jobs.click();
			  
//read the Page Title
	  
	  String pageTitle = driver.findElement(By.className("site-description")).getText();
	  
	  System.out.println("Title of the page is " +pageTitle);
	 
	  
  }

  @AfterMethod
  public void afterMethod() {
	//Close the browser
      driver.quit();
  }
  

}
