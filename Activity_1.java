package TestNGActivities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_1 {
	
	WebDriver driver;
	
	@BeforeMethod
	  public void beforeMethod() {
		//Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		
		 //Open browser
		driver.get("https://alchemy.hguy.co/jobs");	
	
	    }

  @Test
  public void testcase1() {
	// Check the title of the page
	  String title = driver.getTitle();
	//Print the title of the page
		System.out.println("Title of the Page is" +title);
		
		//Assertion for page title
        Assert.assertEquals("Alchemy Jobs � Job Board Application", title);               
  }
  
 
  @AfterMethod
  public void afterMethod() {
	  //Close the browser
      driver.quit();
  }

}
